import json
import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix_server')


@pytest.fixture
def tls_psk(ansible_vars):
    tls_psk = ansible_vars['zabbix_agent_tlspsk_secret']

    return tls_psk

@pytest.fixture
def tls_psk_identity(ansible_vars):
    tls_psk_identity = ansible_vars['zabbix_agent_tlspskidentity']

    return tls_psk_identity

@pytest.fixture
def tls_psk_file(ansible_vars):
    tls_psk_file = ansible_vars['zabbix_agent_tlspskfile']

    return tls_psk_file

def test_psk_host(host, zabbix_auth, tls_psk, get_hosts):
    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "host.get",
            "params": {
                "filter": {
                    "host": get_hosts
                }
            },
            "selectInventory": "extend",
            "auth": zabbix_auth,
            "output": "extend",
            "id": 1
        }
    )

    response = host.run(
        f"curl localhost/api_jsonrpc.php "
        f"-H 'Content-Type: application/json-rpc' -X POST "
        f"--data {json.dumps(encoded_body)}"
        )

    result = json.loads(response.stdout)['result']

    assert result[0]['tls_psk'] == tls_psk
    assert result[0]['tls_psk_identity'] == "test"
    assert result[0]['tls_accept'] == "2"


def test_zabbix_agent_psk(tls_psk, tls_psk_file):
    testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE'])
    zabbix_agent_host = testinfra_hosts.get_host(testinfra_hosts.get_hosts('zabbix_agent')[0])

    psk_file = zabbix_agent_host.file(tls_psk_file)

    assert psk_file.user == "zabbix"
    assert psk_file.group == "zabbix"
    assert psk_file.mode == 0o400
    assert psk_file.contains(tls_psk)
