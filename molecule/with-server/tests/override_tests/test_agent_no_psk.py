import json
import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix_server')


def test_no_psk_host(host, zabbix_auth):
    hostname = host.check_output('hostname -s')
    encoded_body = json.dumps(
        {
            "jsonrpc": "2.0",
            "method": "host.get",
            "params": {
                "filter": {
                    "host": ["zabbix-agent-centos"],
                }
            },
            "selectInventory": "extend",
            "auth": zabbix_auth,
            "output": "extend",
            "id": 1
        }
    )

    response = host.run(
        f"curl localhost/api_jsonrpc.php "
        f"-H 'Content-Type: application/json-rpc' -X POST "
        f"--data {json.dumps(encoded_body)}"
        )

    result = json.loads(response.stdout)['result']

    assert result[0]['tls_accept'] == "1", "TLS is enabled when it shouldn't be"
