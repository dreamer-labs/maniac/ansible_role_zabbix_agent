import json
import os
import pytest
import yaml

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zabbix_server')

# This ansible_vars overrides the one defined above it (../conftest.py)

@pytest.fixture
def ansible_vars():
    pth = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'common', 'playbooks', 'converge_expects_server.yml')
    with open(pth) as ymlfile:
        ansible_vars = yaml.safe_load(ymlfile)[1]['roles'][0]['vars']

    return ansible_vars
